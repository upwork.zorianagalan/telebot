# Installation

## Windows
* [`Download Node JS`](https://nodejs.org/en/download/)
* [`Download Git`](https://git-scm.com/download/win)


## Cloning this repo
```cmd
> git clone https://github.com/LoL-Human/TelegramBot-NodeJS
> cd TelegramBot-NodeJS
```

## Install the package
```cmd
> npm i
```
```

## Run the bot
```cmd
> npm start
```

## Note:
* You can request a case in my [`WhatsApp`](http://wa.me/62895418200111).

# Thanks To
* [`Telegraf`](https://github.com/telegraf/telegraf)